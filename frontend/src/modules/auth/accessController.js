export const PermissionMap = {
  canEditPage: 'canEditPage',
}

export default {
  isUserHasPermission(user, permission) {
    return user && user.permissions.hasOwnProperty(permission) && user.permissions[permission]
  }
}
