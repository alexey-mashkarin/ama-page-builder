import { api } from 'boot/axios'
import { LocalStorage } from 'quasar'
import emiter from 'src/js/emiter'

const storageKey = 'currentUser'

export default {

  login(login, password) {
    const data = {
      login,
      password
    }
    return api.post('/api/v1/auth', data)
      .then((response) => {
          if (response.status === 200) {
            LocalStorage.set(storageKey, JSON.stringify(response.data))
            emiter.emit('onLogin', response.data)
          }
          return response
        },
        (error) => {
          console.error(error);
          throw error;
        }
      )
  },

  isLoggedIn() {
    return LocalStorage.has(storageKey)
  },

  logout() {
    LocalStorage.remove(storageKey)
    emiter.emit('onLogout')
    location.reload()
  },

  getCurrentUser() {
    return LocalStorage.has(storageKey) ? JSON.parse(LocalStorage.getItem(storageKey)) : null;
  },

  onBoot(store) {
    if (this.isLoggedIn()) {
      store.commit('auth/setCurrentUser', this.getCurrentUser())
    }
  }
}
