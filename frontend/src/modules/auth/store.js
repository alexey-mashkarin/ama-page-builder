export default {
  namespaced: true,
  getters: {
    getCurrentUser (state) {
      return state.currentUser
    },
  },
  state: {
    currentUser: null,
  },
  mutations: {
    setCurrentUser (state, currentUser) {
      state.currentUser = currentUser
    },
  },
  actions: {
    setCurrentUser (context, currentUser) {
      context.commit('setCurrentUser', currentUser)
    },
  },
}
