import { api } from 'boot/axios'

export default {
  fetchUserList () {
    return api.get('/api/v1/users').then(response => response.data)
  },
  fetchPageList () {
    return api.get('/api/v1/pages').then(response => response.data)
  },
  fetchPageData (pageUrl) {
    return api.get('/api/v1/page', {
      params: {
        url: pageUrl
      }
    }).then(response => response.data)
  },
}
