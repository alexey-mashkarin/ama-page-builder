import MockAdapter from 'axios-mock-adapter'
import userList from './data/userList.json'
import pagesTree from './data/pagesTree.json'
import pageData from './data/pageData.json'

export default {
  apply (api) {
    let mock = new MockAdapter(api, { delayResponse: 1000 })
    let adminUser = userList[0]

    mock.onGet()
    mock.onPost('/api/v1/auth',
      { login: adminUser.login, password: adminUser.password }).reply(200, {
      ...adminUser,
      token: 'fake-jwt-token-admin-' + (new Date()).getTime(),
    })
    mock.onPost('/api/v1/auth').reply(401, {
      message: 'Unauthorized',
    })

    mock.onGet('/api/v1/users').reply(200, userList)
    mock.onGet('/api/v1/pages').reply(200, pagesTree)
    mock.onGet('/api/v1/page').reply((config) => {

      if (config.params.hasOwnProperty('url')) {

        const url = config.params.url
        let page = null

        pageData.forEach((item) => {
          if (item.settings.props.url === url) {
            page = item
          }
        })

        if (!page) {
          return 404
        }

        return [
          200,
          page,
        ]
      }
      return 400
    })

  },
}
