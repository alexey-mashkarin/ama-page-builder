import Col from 'components/grids/Col'
import Row from 'components/grids/Row'

export default {
  Col,
  Row,
}
