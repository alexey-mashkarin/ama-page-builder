import Editor from 'components/propertyViews/Editor'
import Text from 'components/propertyViews/Text'
import Textarea from 'components/propertyViews/Textarea'

export default {
  Editor,
  Text,
  Textarea,
}
