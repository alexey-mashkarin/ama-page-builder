import { ref, onMounted, onUnmounted, reactive } from 'vue'
import client from 'src/api/client'
import { useMeta } from 'quasar'
import emiter from 'src/js/emiter'
import { PageType } from 'src/js/types'

export function usePageData() {

  const data = reactive({
    pageData: PageType
  })

  client.fetchPageData(location.pathname).then((response) => {
    data.pageData = response;
  })

  return data.pageData
}
