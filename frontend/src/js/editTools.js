import { ref } from 'vue'
import { LocalStorage } from 'quasar'

export const editModeTools = {
  editElements: 'editElements',
  editGrids: 'editGrids',
}

const editMode = ref(LocalStorage.getItem('editMode') || false)
const editTools = ref({
  editElements: false,
  editGrids: false,
})

if (editMode.value) {
  editTools.value [editMode.value] = true
}

export function useEditTools() {
  return editTools
}

export function useEditMode() {
  return editMode
}

export function toggleEditMode(editToolKey) {

  if (!editTools.value.hasOwnProperty(editToolKey)) {
    return
  }

  if (editTools.value[editToolKey] === false) {
    editMode.value = null
    LocalStorage.set('editMode', null)
    return
  }


  if (editMode.value && editMode.value !== editToolKey && editTools.value[editMode.value]) {
    editTools.value[editMode.value] = false
  }
  editMode.value = editToolKey
  LocalStorage.set('editMode', editToolKey)

}
