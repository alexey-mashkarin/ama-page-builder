export const PageType = {
  id: 0,
  settings: {
    metaData: {},
    props: {},
  },
  childrenGrids: [],
  childrenElements: [],
  gridMap: {},
  elementMap: {},
}

export const PageSettingsType = {
  props: {
    url: '',
  },
  metaData: {
    title: '',
  },
}

export const ElementType = {
  type: '',
  data: {},
}

