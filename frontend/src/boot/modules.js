import { boot } from 'quasar/wrappers'
import auth from 'modules/auth'

export default boot(({store}) => {
  auth.onBoot(store)
})
