const routes = [
  {
    path: '/',
    component: () => import('layouts/Default.vue'),
    children: [
      { path: 'login', component: () => import('pages/Login.vue') },
      { path: 'users', component: () => import('pages/UserList.vue') },
      { path: '', component: () => import('pages/Page.vue') },
      { path: ':catchAll(.*)*', component: () => import('pages/Page.vue') },
    ],
  },
  {
    path: '/404',
    component: () => import('pages/Error404.vue'),
  },
]

export default routes
