import { store } from 'quasar/wrappers'
import { createStore } from 'vuex'

import users from './users'
import auth from 'modules/auth/store'
import { PageType } from 'src/js/types'

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default store(function (/* { ssrContext } */) {
  const Store = createStore({
    modules: {
      users,
      auth,
    },
    getters: {
      getPageData (state) {
        return state.pageData
      },
    },
    state: {
      pageData: PageType,
    },
    mutations: {
      setPageData(state, pageData) {
        state.pageData = pageData
      },
    },
    actions: {
      setPageData (context, pageData) {
        context.commit('setPageData', pageData)
      },
    },
    // enable strict mode (adds overhead!)
    // for dev mode and --debug builds only
    strict: process.env.DEBUGGING,
  })

  return Store
})
