import client from '../../api/client'

export default {
  namespaced: true,
  getters: {
    getUserList(state) {
      return state.userList
    }
  },
  state: {
    userList: [],
  },
  mutations: {
    setUserList (state, userList) {
      state.userList = userList
    },
  },
  actions: {
    fetchUserList ({ commit }) {
      return client.fetchUserList().
        then(userList => commit('setUserList', userList))
    },
  },
}
